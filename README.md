# Verbalist
##### Team: Archana Rajkumar, Navin Pai

### About
Verbalist is a voice powered notes/to-do application powered by Kookoo API. Rather than have to keep a note taking app (**Keep/Evernote/Wunderlist etc.**) and having to constantly sync between multiple devices, Verbalist provides a single location to store all your notes.

### Tech
It is powered by the following technology
- **Kookoo API** - for phone related functionality
- **(IBM Watson + Google) Speech to Text API** - For Speech to text functionality
- **MongoDB** - For persistent data story
- **Heroku** - For hosting the web application

### Demo
You can check out the app functionality by calling **02066899120**. The user phone number is used to authenticate the user.

### Version
1.0

### Future Work
- Tie in with SMS for getting todo list as a message
- Web based application to allow phone-free access to your list.


License
----

[WTFPL][wtfpl]

**Created for the [Kookoo Women's Hackathon][kookoo]**

   [kookoo]: <http://www.venturesity.com/challenge/id/173>
   [wtfpl]: <http://www.wtfpl.net/txt/copying/>
