package org.hackathon;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.ozonetel.kookoo.CollectDtmf;
import com.ozonetel.kookoo.Record;
import com.ozonetel.kookoo.Response;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import javazoom.jl.decoder.JavaLayerException;
import org.bson.Document;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javazoom.jl.converter.Converter;

/**
 * Created by archana on 2/4/16.
 */
@WebServlet(name = "verbalist", urlPatterns = {"/verbalist"})
public class Main extends HttpServlet {
  /**
   * @param request  servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException      if an I/O error occurs
   */

  public String MONGO_URL = "mongodb://<user>:<pass>@<host>";
  public String MONGO_DB_NAME = "<dbname>";
  public String MONGO_COLLECTION = "<collection_name>";

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/xml;charset=UTF-8");
    Response r = new Response();

    String event = request.getParameter("event");
    String phone;
    if (request.getParameter("cid").length() == 10)
      phone = "91" + request.getParameter("cid");
    else
      phone = request.getParameter("cid");
    String filename = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());


    if ((null != event) && event.equalsIgnoreCase("NewCall")) {
      r.addPlayText("Welcome to Verbalist");
      CollectDtmf cd = new CollectDtmf(1, "#", 5);
      cd.addPlayText("Press 1 to add an item to list");
      cd.addPlayText("Press 2 to listen to your list");
      cd.addPlayText("Press 3 to clear your list");
      r.addCollectDtmf(cd);

    } else if ((null != event) && event.equalsIgnoreCase("GotDTMF")) {
      String choice = request.getParameter("data");
      if (Integer.parseInt(choice) == 1) {

        r.addPlayText("Speak after the beep");
        Record rec = new Record();
        rec.setFormat("wav");
        rec.setSilence(2);
        rec.setFileName(filename);
        rec.setMaxDuration(15);
        r.addRecord(rec);
      } else if (Integer.parseInt(choice) == 2) {
        MongoCollection<Document> m = getCollection();
        BasicDBObject whereQuery = new BasicDBObject();
        whereQuery.put("phone", phone);
        List<Document> foundDocument = m.find(whereQuery).into(new ArrayList<Document>());
        if (foundDocument.size() == 0) {
          r.addPlayText("your list is empty");
        } else {
          r.addPlayText("Items in your to do list are");
          for (Document i : foundDocument) {
            r.addPlayText(i.getString("item"));
          }
        }
      } else if (Integer.parseInt(choice) == 3) {
        MongoCollection<Document> m = getCollection();
        m.deleteMany(new Document("phone", phone));
        r.addPlayText("Your list has been cleared");
        r.addHangup();
      } else {
        CollectDtmf cd = new CollectDtmf(1, "#", 5);
        cd.addPlayText("Invalid Value. Please Try Again.");
        r.addCollectDtmf(cd);
      }
    } else if ((null != event) && event.equalsIgnoreCase("Record")) {
      String file_url = request.getParameter("data");
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      MongoCollection<Document> m = getCollection();
      URL audioFile = new URL(file_url);
      ReadableByteChannel rbc;
      try {
        rbc = Channels.newChannel(audioFile.openStream());
      } catch (FileNotFoundException ex) {
        try {
          Thread.sleep(1500);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      rbc = Channels.newChannel(audioFile.openStream());
      FileOutputStream fos = new FileOutputStream(filename + ".mp3");
      fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
      fos.close();

      Converter convert = new Converter();
      try {
        convert.convert(filename + ".mp3", filename + ".wav");
      } catch (JavaLayerException e) {
        e.printStackTrace();
      }

      SpeechToText s = new SpeechToText();
      String recognizedString = s.recognize(filename + ".wav", "GOOGLE");
      m.insertOne(new Document().append("phone", phone).append("item", recognizedString));
      r.addPlayText(recognizedString);
      r.addPlayText("Has been added to your list");
      r.addHangup();
    }
    String kookooResponseOutput = r.getXML();
    response.getWriter().write(kookooResponseOutput);
  }

  private MongoCollection<Document> getCollection() {
    MongoClientURI uri = new MongoClientURI(MONGO_URL);

    MongoClient mongoClient = new MongoClient(uri);
    MongoDatabase database = mongoClient.getDatabase(MONGO_DB_NAME);
    return database.getCollection(MONGO_COLLECTION);
  }
}