package org.hackathon;

import com.darkprograms.speech.recognizer.GoogleResponse;
import com.darkprograms.speech.recognizer.Recognizer;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by navin on 2/6/16.
 */
public class SpeechToText {

  public String GOOGLE_API_KEY = "<GOOGLE_API_KEY>";
  public String WATSON_USER_PASS = "<WATSON_USER:WATSON_PASS>";

  public String recognize(String wavfile, String ttsEngine){
    if(ttsEngine.equals("WATSON")){
      return recognizeWatson(wavfile);
    }
    else {
      return recognizeGoogle(wavfile);
    }
  }
  public String recognizeGoogle(String wavfile){
    //GOOGLE RECOGNITION
    Recognizer recognizer = new Recognizer(Recognizer.Languages.ENGLISH_US, GOOGLE_API_KEY);

    try {
      int maxNumOfResponses = 4;
      GoogleResponse response = recognizer.getRecognizedDataForWave(wavfile, maxNumOfResponses);
      return response.getResponse();
    } catch (Exception ex) {
      System.out.println("ERROR: Google cannot be contacted");
      ex.printStackTrace ();
      return "Internal error";
    }
  }

  public String recognizeWatson(String wavfile) {
   // IBM WATSON
    try {

      String url = "https://stream.watsonplatform.net/speech-to-text/api/v1/recognize?continuous=true";

      URL obj = new URL(url);
      HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

      conn.setRequestProperty("Content-Type", "audio/wav");
      conn.setRequestProperty("Transfer-Encoding", "chunked");
      conn.setDoOutput(true);
      conn.setDoInput(true);

      conn.setRequestMethod("POST");

      String userpass = WATSON_USER_PASS;
      String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes("UTF-8"));
      conn.setRequestProperty("Authorization", basicAuth);


      OutputStream output;

      try {
        File in = new File(wavfile);
        output = conn.getOutputStream();
        InputStream is = new FileInputStream(in);
        BufferedInputStream bis = new BufferedInputStream(is, 16000);
        DataInputStream dis = new DataInputStream(bis);
        copyStream(dis, output);
      } catch (Exception e) {
        System.out.println("Error");
      }

      conn.connect();
      int responseCode = conn.getResponseCode();

      if (responseCode == HttpURLConnection.HTTP_OK) {
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
          response.append(inputLine);
        }
        in.close();

        JSONObject responseString= new JSONObject(response.toString());
        return responseString.getJSONArray("results").getJSONObject(0).getJSONArray("alternatives").getJSONObject(0).get("transcript").toString();
      }
      else{
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
          response.append(inputLine);
        }
        in.close();

        JSONObject responseString= new JSONObject(response.toString());
        return responseString.getString("error");

      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "error happened";
  }

  public void copyStream(InputStream is, OutputStream os) {
    final int buffer_size = 4096;
    try {

      byte[] bytes = new byte[buffer_size];
      int k = -1;
      while ((k = is.read(bytes, 0, bytes.length)) > -1) {
        if (k != -1) {
          os.write(bytes, 0, k);
        }
      }
      os.flush();
      is.close();
      os.close();
    } catch (Exception ex) {

    }
  }
}